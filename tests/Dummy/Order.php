<?php


namespace Ollegio\Dto\Tests\Dummy;


use Ollegio\Dto\Dto;

class Order extends Dto
{
    public function __construct(
        public int $id,
        public float $total,
        public array $products = [],
    ) {
    }

    public static function fromArray(array $data): static
    {
        $data['products'] = array_map(fn($p) => Product::fromArray($p), $data['products']);
        return parent::fromArray($data);
    }
}