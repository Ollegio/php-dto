<?php


namespace Ollegio\Dto\Tests\Dummy;


use Ollegio\Dto\Dto;

class SimpleDto extends Dto
{
    public function __construct(
        public int $number,
        public string $name,
    ) {
    }
}