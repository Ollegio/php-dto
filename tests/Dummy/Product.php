<?php


namespace Ollegio\Dto\Tests\Dummy;


use Ollegio\Dto\Dto;

class Product extends Dto
{
    public function __construct(
        public string $name,
        public float $price,
    ) {
    }
}