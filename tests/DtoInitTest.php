<?php

namespace Ollegio\Dto\Tests;

use Ollegio\Dto\Tests\Dummy\Order;
use Ollegio\Dto\Tests\Dummy\SimpleDto;
use PHPUnit\Framework\TestCase;

class DtoInitTest extends TestCase
{
    public function testInit()
    {
        $data = [
            'number' => 42,
            'name' => 'John',
        ];

        $dto = new SimpleDto(...$data);
        $this->assertEquals($data, $dto->toArray());
    }

    public function testFromArray()
    {
        $data = [
            'number' => 42,
            'name' => 'John',
        ];

        $dto = SimpleDto::fromArray($data);
        $this->assertEquals($data, $dto->toArray());
    }

    public function testFromArrayNested()
    {
        $data = [
            'id' => 357,
            'total' => 200,
            'products' => [
                [
                    'name' => 'product 1',
                    'price' => 150,
                ],
                [
                    'name' => 'product 2',
                    'price' => 50,
                ],
            ],
        ];

        $dto = Order::fromArray($data);
        $this->assertCount(count($data['products']), $dto->products);
        $this->assertEquals($data['products'][0]['price'], $dto->products[0]->price);
        $this->assertEquals($data['products'][1]['price'], $dto->products[1]->price);
    }
}