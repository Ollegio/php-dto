<?php

namespace Ollegio\Dto;

use JsonSerializable;

class Dto implements JsonSerializable
{
    public function toArray(): array
    {
        return array_map([$this, 'convert'], get_object_vars($this));
    }

    private function convert($prop): mixed
    {
        return match (true) {
            $prop instanceof self => $prop->toArray(),
            is_array($prop) => array_map([$this, 'convert'], $prop),
            default => $prop,
        };
    }

    public function jsonSerialize(): array
    {
        return $this->toArray();
    }

    public static function fromArray(array $data): static
    {
        $data = array_intersect_key($data, get_class_vars(static::class));
        return new static(...$data);
    }
}
